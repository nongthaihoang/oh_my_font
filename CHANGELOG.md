### 2025-02-22
- Added bootloop prevention:  
  The module will disable itself after 60s if your phone has not finished booting up yet.  
  Restart your phone and the module will be disabled.

#### _— Previous Version —_

#### 2025-01-12
- Updated config: Removed the REBOOT option for easier troubleshooting.  
  Your config will be RESET!
- Added support for OnePlus 13 Android 15.
- Replaced new 2 font familes `google-sans-flex` and `roboto-flex` for better compatibility (just in case).

#### 2025-01-06
- When UPDATING, the installation will be ABORTED if it can NOT get the original fontxml to prevent bootloop/problems.  
  In this case, please remove the module first then install it as usual.

#### 2025-01-04
- LOS Android 15: Fixed missing characters due to missing Roboto font (wrong index).
- Config: Removed/Hid GS option. Since Android 15, the option does not work as it should.  
  Your config will be RESET!
- Fixed OTL does not work for some apps, e.g. Google Keep.
- Removed an old optical sizes tweak that is not suitable anymore.

#### 2025-01-02
- Fixed missing/incorrect fonts when enable OTL and dirty flash.

#### 2024-12-27
- Allowed to update the module (without removing it first)

#### 2024-12-26
- Updated config: added preview links for variable fonts. Your config will be RESET!

#### 2024-12-23
- Improved support for Android 15: Patching both `fonts.xml` and `font_fallback.xml` separately.

#### 2024-12-22
- Added support for Android 15 (`font_fallback.xml`)
- Updated latest San Francisco and New York font (iOS 18)

[Full Changelog](https://gitlab.com/nongthaihoang/oh_my_font/-/commits/master)
